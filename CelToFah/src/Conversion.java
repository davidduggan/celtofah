
public class Conversion 
{
	public Conversion()
	{
	}
	
	double res1, res2 = 0;
	
	public double CelsiusToFahrenheit(double Celsius) throws ConversionException
	{
			if(Celsius < -273 && Celsius > 100)
			{
				throw new ConversionException("Please enter a valid number between -273 and 100");
			}
			else 
			{
				res1 = (Celsius * 9/5) + 32;
			}
			
		return res1;
	}
	
	public double FahrenheitToCelsius(double Fahrenheit) throws ConversionException
	{ 
			if(Fahrenheit < -273 && Fahrenheit > 100)
			{
				throw new ConversionException("Please enter a valid number between -459 and 212");
			}
			else 
			{
				res2 = (Fahrenheit - 32) * 5/9;
			}
		return res2;
	}
}
