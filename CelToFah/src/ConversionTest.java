import junit.framework.TestCase;

public class ConversionTest extends TestCase 
{
	//CelsiusToFahrenheit Method
	
	//EP
	
	//Test #: 1
	//Test Objective: Numbers from -273 - 100
	//Test Input(s): 55
	//Test Expected Output(s): 131.00
	
	public void testCelsiusToFahrenheit001()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			assertEquals(131.00, testObj.CelsiusToFahrenheit(55), 2); 
		}
		catch(ConversionException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 2
	//Test Objective: less than -273
	//Test Input(s): -333
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -273 and 100" message
	
	public void testCelsiusToFahrenheit002()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res1 = testObj.CelsiusToFahrenheit(-333.0);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -273 and 100", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 3
	//Test Objective: Numbers above 100
	//Test Input(s): 154
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -273 and 100" message
	
	public void testCelsiusToFahrenheit003()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res1 = testObj.CelsiusToFahrenheit(154);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -273 and 100", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//BVA
	
	//Test #: 4
	//Test Objective: -273.01
	//Test Input(s): -273.01
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -273 and 100" message

	public void testCelsiusToFahrenheit004()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res1 = testObj.CelsiusToFahrenheit(-273.01);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -273 and 100", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 5
	//Test Objective: Over 100
	//Test Input(s): 100.01
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -273 and 100" message

	public void testCelsiusToFahrenheit005()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res1 = testObj.CelsiusToFahrenheit(100.01);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -273 and 100", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 6
	//Test Objective: MINDouble
	//Test Input(s): Double.MIN_VALUE
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -273 and 100" message 
	
	public void testCelsiusToFahrenheit006()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res1 = testObj.CelsiusToFahrenheit(Double.MIN_VALUE);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -273 and 100", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 7
	//Test Objective: MAXDouble
	//Test Input(s): Double.MAX_VALUE
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -273 and 100" message 
	
	public void testCelsiusToFahrenheit007()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res1 = testObj.CelsiusToFahrenheit(Double.MAX_VALUE);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -273 and 100", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 8
	//Test Objective: Value of -273
	//Test Input(s): -273
	//Test Expected Output(s): -459.40
	
	public void testCelsiusToFahrenheit008()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			assertEquals(-459.40, testObj.CelsiusToFahrenheit(-273), 2); 
		}
		catch(ConversionException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 9
	//Test Objective: Value of 100
	//Test Input(s): 100
	//Test Expected Output(s): 212.00 
	
	public void testCelsiusToFahrenheit009()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			assertEquals(212.00, testObj.CelsiusToFahrenheit(100), 2); 
		}
		catch(ConversionException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//FahrenheitToCelsius Method
	
	//EP
	
	//Test #: 1
	//Test Objective: Numbers from -459 - 212
	//Test Input(s): 3
	//Test Expected Output(s): -16.11
	
	public void testFahrenheitToCelsius001()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			assertEquals(-16.11, testObj.FahrenheitToCelsius(3), 2); 
		}
		catch(ConversionException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 2
	//Test Objective: less than -459
	//Test Input(s): -489
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -459 and 212" message
	
	public void testFahrenheitToCelsius002()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res2 = testObj.FahrenheitToCelsius(-489);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -459 and 212", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 3
	//Test Objective: more than 212
	//Test Input(s): 222
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -459 and 212" message
	
	public void testFahrenheitToCelsius003()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res2 = testObj.FahrenheitToCelsius(222);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -459 and 212", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//BVA
	
	//Test #: 4
	//Test Objective: more than 212
	//Test Input(s): 212.01
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -459 and 212" message
	
	public void testFahrenheitToCelsius004()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res2 = testObj.FahrenheitToCelsius(212.01);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -459 and 212", e.getMessage());
			fail("Please enter a valid number between -273 and 100");
		}
	}
	
	//Test #: 5
	//Test Objective: less than -459
	//Test Input(s): -459.01
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -459 and 212" message
	
	public void testFahrenheitToCelsius005()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res2 = testObj.FahrenheitToCelsius(-459.01);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -459 and 212", e.getMessage());
			fail("Please enter a valid number between -459 and 212");
		}
	}
	
	//Test #: 6
	//Test Objective: MINDouble
	//Test Input(s): Double.MIN_VALUE
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -459 and 212" message 
	
	public void testFahrenheitToCelsius006()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res2 = testObj.FahrenheitToCelsius(Double.MIN_VALUE);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -459 and 212", e.getMessage());
			fail("Please enter a valid number between -459 and 212");
		}
	}
	
	//Test #: 7
	//Test Objective: MAXDouble
	//Test Input(s): Double.MAX_VALUE
	//Test Expected Output(s): Exception thrown with "Please enter a valid number between -459 and 212" message 
	
	public void testFahrenheitToCelsius007()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			double res2 = testObj.FahrenheitToCelsius(Double.MIN_VALUE);
		}
		catch(ConversionException e)
		{
			assertEquals("Please enter a valid number between -459 and 212", e.getMessage());
			fail("Please enter a valid number between -459 and 212");
		}
	}
	
	//Test #: 8
	//Test Objective: Value of -459
	//Test Input(s): -459
	//Test Expected Output(s): -272.778
	
	public void testFahrenheitToCelsius008()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			assertEquals(-272.77, testObj.FahrenheitToCelsius(-459), 2); 
		}
		catch(ConversionException e)
		{
			fail("No Exception Expected");
		}
	}
	
	//Test #: 9
	//Test Objective: Value of 212
	//Test Input(s): 212
	//Test Expected Output(s): 100.00
	
	public void testFahrenheitToCelsius009()
	{
		Conversion testObj = new Conversion();
		
		try
		{
			assertEquals(100.00, testObj.FahrenheitToCelsius(212), 2); 
		}
		catch(ConversionException e)
		{
			fail("No Exception Expected");
		}
	}
}