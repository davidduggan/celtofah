
public class ConversionException extends Exception 
{
	String message;
	
	public ConversionException(String errMessage)
	{
		message = errMessage;
	}
	
	public String getMessage() 
	{
		return message;
	}
}
